using System.Collections;
using System.Collections.Generic;
//using Unity.VisualScrip;
using Unity.VisualScripting;
using UnityEngine;

[CreateAssetMenu(fileName = "newIdleStateData", menuName = "Data/Idle Data/Idle State")]
public class D_IdleState : ScriptableObject
{
    public float minIdleTime = 1f;
    public float maxIdleTime = 2f;
}
