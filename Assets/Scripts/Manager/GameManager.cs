﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField]
    private Transform respawnPoint;
    [SerializeField]
    private GameObject player;
    [SerializeField]
    private float respawnTime;

    private float respawnTimeStart;

    private bool respawn;


    private CinemachineVirtualCamera CVC;

    [SerializeField]
    private EnemySpawner enemySpawner; // Reference to the EnemySpawner script

    private void Start()
    {
        CVC = GameObject.Find("Player Camera").GetComponent<CinemachineVirtualCamera>();
        timeRemaining = timeLimit;
        UpdateTimeRemainingText();
    }

    private void Update()
    {
        CheckRespawn();
        if (!isGameOver)
        {
            UpdateTimeRemaining();
        }
    }

    public void Respawn()
    {
        respawnTimeStart = Time.time;
        respawn = true;
    }

    private void CheckRespawn()
    {
        if (Time.time >= respawnTimeStart + respawnTime && respawn)
        {
            var playerTemp = Instantiate(player, respawnPoint);
            CVC.m_Follow = playerTemp.transform;
            respawn = false;

            // Call the enemy spawning function from the EnemySpawner script
            enemySpawner.SpawnEnemy();
        }
    }

    public static GameManager instance;
    public Text txtPoint;
    public Text txtSpawnedEnemies; // Add a reference to the UI Text element for spawned enemies

    private int point = 0;
    private int spawnedEnemies = 0; // Add a variable to track spawned enemies

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void UpdatePoints(int additionalPoints)
    {
        point += additionalPoints;
        txtPoint.text = "Points: " + point;
    }

    public void UpdateSpawnedEnemies(int additionalEnemies)
    {
        spawnedEnemies += additionalEnemies;
        txtSpawnedEnemies.text = "Spawned Enemies: " + spawnedEnemies;
    }

    public Text timeRemainingText;
    public float timeLimit = 60.0f; // Adjust the time limit as needed
    private float timeRemaining;
    private bool isGameOver = false;
    public Text gameOverText;
    private void UpdateTimeRemaining()
    {
        timeRemaining -= Time.deltaTime;
        if (timeRemaining <= 0)
        {
            timeRemaining = 0;
            isGameOver = true;
            // You can add game over logic here, like showing a game over screen.
            //gameOverText.text = "Game Over";
            //Time.timeScale = 0;
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
        UpdateTimeRemainingText();
    }


    


   

    private void UpdateTimeRemainingText()
    {
        timeRemainingText.text = "Time Remaining: " + Mathf.Ceil(timeRemaining).ToString(); // Display remaining time as an integer
    }
}
