using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField]
    private GameObject enemyPrefab; // The enemy prefab you want to spawn
    [SerializeField]
    private Transform[] spawnPoints; // An array of spawn points where enemies can appear
    [SerializeField]
    private float initialSpawnInterval = 2.0f; // Initial time between enemy spawns
    [SerializeField]
    private float spawnRateIncrease = 1.0f; // Rate of increase in spawn rate per second

    private float nextSpawnTime;
    private int numberOfEnemiesToSpawn = 1;

    private void Start()
    {
        nextSpawnTime = Time.time + initialSpawnInterval;
    }

    private void Update()
    {
        if (Time.time >= nextSpawnTime)
        {
            for (int i = 0; i < numberOfEnemiesToSpawn; i++)
            {
                SpawnEnemy();
            }

            UpdateSpawnRate();
            nextSpawnTime = Time.time + initialSpawnInterval;
        }
    }

    private void UpdateSpawnRate()
    {
        initialSpawnInterval -= spawnRateIncrease * Time.deltaTime;
        initialSpawnInterval = Mathf.Max(initialSpawnInterval, 0.5f); // Set a minimum spawn interval
        numberOfEnemiesToSpawn++; // Increase the number of enemies to spawn
    }

    public void SpawnEnemy()
    {
        if (spawnPoints.Length == 0 || enemyPrefab == null)
        {
            Debug.LogWarning("Spawn points or enemy prefab not set up correctly!");
            return;
        }

        // Randomly select a spawn point
        Transform spawnPoint = spawnPoints[Random.Range(0, spawnPoints.Length)];

        // Instantiate the enemy at the selected spawn point
        Instantiate(enemyPrefab, spawnPoint.position, Quaternion.identity);
    }
}
